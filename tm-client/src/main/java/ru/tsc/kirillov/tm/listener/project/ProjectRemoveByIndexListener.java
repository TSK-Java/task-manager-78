package ru.tsc.kirillov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;
import ru.tsc.kirillov.tm.dto.request.ProjectRemoveByIndexRequest;
import ru.tsc.kirillov.tm.dto.response.ProjectRemoveByIndexResponse;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kirillov.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIndexListener extends AbstractProjectTaskListener {

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удалить проект по его индексу.";
    }

    @Override
    @EventListener(condition = "@projectRemoveByIndexListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Удаление проекта по индексу]");
        System.out.println("Введите индекс проекта:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(getToken(), index);
        @NotNull final ProjectRemoveByIndexResponse response = getProjectTaskEndpoint().removeProjectByIndex(request);
        @Nullable final ProjectDto project = response.getProject();
        if (project == null) throw new ProjectNotFoundException();
    }

}
