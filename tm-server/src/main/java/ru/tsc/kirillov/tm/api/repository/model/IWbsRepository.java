package ru.tsc.kirillov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import ru.tsc.kirillov.tm.model.AbstractWbsModel;

import java.util.List;

@NoRepositoryBean
public interface IWbsRepository<M extends AbstractWbsModel> extends IUserOwnedRepository<M> {

    @NotNull
    List<M> findAll(@NotNull Sort sort);

    @NotNull
    List<M> findAllByUserId(@Nullable String userId, @NotNull Sort sort);

}
