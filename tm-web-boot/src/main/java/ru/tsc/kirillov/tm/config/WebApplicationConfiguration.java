package ru.tsc.kirillov.tm.config;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.tsc.kirillov.tm.endpoint.AuthEndpointImpl;
import ru.tsc.kirillov.tm.endpoint.ProjectEndpointImpl;
import ru.tsc.kirillov.tm.endpoint.TaskEndpointImpl;
import ru.tsc.kirillov.tm.enumerated.RoleType;

import javax.xml.ws.Endpoint;

@Configuration
@EnableJpaAuditing
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebApplicationConfiguration extends WebSecurityConfigurerAdapter {

    @NotNull
    @Autowired
    private Bus bus;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    public AuthenticationManager authenticationManagerBean()
            throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public Endpoint authEndpointRegistry(
            @NotNull final AuthEndpointImpl authEndpoint,
            @NotNull final SpringBus cxf
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, authEndpoint);
        endpoint.publish("/AuthEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint projectEndpointRegistry(
            @NotNull final ProjectEndpointImpl projectEndpoint,
            @NotNull final SpringBus cxf
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(
            @NotNull final TaskEndpointImpl taskEndpoint,
            @NotNull final SpringBus cxf
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Override
    protected void configure(@NotNull final HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/services/*").permitAll()
                .antMatchers("/api/auth/login").permitAll()

                .and()
                .authorizeRequests()

                .anyRequest().hasAnyRole(RoleType.USUAL.name(), RoleType.ADMIN.name())

                .and()
                .formLogin()
                .and()
                .logout().permitAll()
                .logoutSuccessUrl("/login")
                .and()
                .csrf().disable();
    }

}
