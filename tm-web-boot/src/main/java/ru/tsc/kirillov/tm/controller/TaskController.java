package ru.tsc.kirillov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.kirillov.tm.api.service.IProjectService;
import ru.tsc.kirillov.tm.api.service.ITaskService;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;
import ru.tsc.kirillov.tm.dto.model.TaskDto;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.util.UserUtil;

import java.util.Collection;

@Controller
public class TaskController {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Nullable
    private Collection<TaskDto> getTasks() {
        return taskService.findAll(UserUtil.getUserId());
    }

    @Nullable
    private Collection<ProjectDto> getProjects() {
        return projectService.findAll(UserUtil.getUserId());
    }

    @NotNull
    private Status[] getStatuses() {
        return Status.values();
    }

    @NotNull
    @GetMapping("/task/create")
    public String create() {
        taskService.create(UserUtil.getUserId());
        return "redirect:/tasks";
    }

    @NotNull
    @GetMapping("/task/delete/{id}")
    public String delete(@NotNull @PathVariable("id") final String id) {
        taskService.removeById(UserUtil.getUserId(), id);
        return "redirect:/tasks";
    }

    @NotNull
    @PostMapping("/task/edit/{id}")
    public String edit(
            @NotNull @ModelAttribute("task") final TaskDto task,
            @NotNull final BindingResult result
    ) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskService.save(UserUtil.getUserId(), task);
        return "redirect:/tasks";
    }

    @NotNull
    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@NotNull @PathVariable("id") final String id) {
        @NotNull final TaskDto task = taskService.findById(UserUtil.getUserId(), id);
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", getProjects());
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    @NotNull
    @GetMapping("/tasks")
    public ModelAndView list() {
        return new ModelAndView("task-list", "tasks", getTasks());
    }
    
}
