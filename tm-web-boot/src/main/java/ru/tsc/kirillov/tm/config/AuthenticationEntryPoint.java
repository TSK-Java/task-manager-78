package ru.tsc.kirillov.tm.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import ru.tsc.kirillov.tm.dto.model.Message;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class AuthenticationEntryPoint extends BasicAuthenticationEntryPoint {

    @Override
    public void commence(
            @NotNull final HttpServletRequest request,
            @NotNull final HttpServletResponse response,
            @NotNull final AuthenticationException authException
    ) throws IOException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

        @NotNull final PrintWriter writer = response.getWriter();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String value = authException.getMessage();
        @NotNull final Message message = new Message(value);
        writer.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(message));
    }

    @Override
    public void afterPropertiesSet() {
        setRealmName("kirillov");
        super.afterPropertiesSet();
    }

}
