package ru.tsc.kirillov.tm.exception.user;

public final class UserLoginEmptyException extends AbstractUserException {

    public UserLoginEmptyException() {
        super("Ошибка! Логин пользователя не задан.");
    }

}
