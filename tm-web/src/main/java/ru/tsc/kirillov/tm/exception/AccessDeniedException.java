package ru.tsc.kirillov.tm.exception;

public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Ошибка! Доступ запрещён.");
    }

}
