package ru.tsc.kirillov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kirillov.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.kirillov.tm.api.service.ITaskService;
import ru.tsc.kirillov.tm.dto.model.TaskDto;
import ru.tsc.kirillov.tm.exception.AccessDeniedException;
import ru.tsc.kirillov.tm.exception.entity.EntityEmptyException;
import ru.tsc.kirillov.tm.exception.field.IdEmptyException;
import ru.tsc.kirillov.tm.exception.field.NameEmptyException;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private ITaskDtoRepository repository;

    @NotNull
    @Override
    @Transactional
    public TaskDto create(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final TaskDto task = new TaskDto(
                userId,
                "Новая задача: " + System.currentTimeMillis(),
                "Описание",
                new Date(),
                new Date());
        save(userId, task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDto create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final TaskDto task = new TaskDto(
                userId,
                name,
                "Описание",
                new Date(),
                new Date());
        save(userId, task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDto save(@Nullable final String userId, @Nullable final TaskDto task) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (task == null) throw new EntityEmptyException();
        task.setUserId(userId);
        return repository.save(task);
    }

    @Nullable
    @Override
    public Collection<TaskDto> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public TaskDto findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findFirstByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final TaskDto task) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (task == null) throw new EntityEmptyException();
        removeById(userId, task.getId());
    }

    @Override
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final List<TaskDto> tasks) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (tasks == null) throw new EntityEmptyException();
        tasks
                .stream()
                .forEach(task -> remove(userId, task));
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        repository.deleteByUserId(userId);
    }

    @Override
    public long count(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return repository.countByUserId(userId);
    }
    
}
