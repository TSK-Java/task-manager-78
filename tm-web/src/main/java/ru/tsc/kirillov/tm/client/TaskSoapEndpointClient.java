package ru.tsc.kirillov.tm.client;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.endpoint.ITaskEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class TaskSoapEndpointClient {

    public static ITaskEndpoint getInstance(@NotNull final String baseURL) throws MalformedURLException {
        @NotNull final String wsdl = baseURL + "/ws/TaskEndpoint?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "TaskEndpointImplService";
        @NotNull final String ns = "http://endpoint.tm.kirillov.tsc.ru/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final ITaskEndpoint result = Service.create(url, name).getPort(ITaskEndpoint.class);
        @NotNull final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return result;
    }

}
