package ru.tsc.kirillov.tm.dto.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

@Getter
@Setter
public class UserEx extends User {

    @Nullable
    private String userId = null;

    @Nullable
    private String email = null;

    public UserEx(@NotNull final User user) {
        super(user.getUsername(),
                user.getPassword(),
                user.isEnabled(),
                user.isAccountNonExpired(),
                user.isCredentialsNonExpired(),
                user.isAccountNonLocked(),
                user.getAuthorities()
        );
    }

    public UserEx(@NotNull final User user, @NotNull final UserDto userDto) {
        super(user.getUsername(),
                user.getPassword(),
                user.isEnabled(),
                user.isAccountNonExpired(),
                user.isCredentialsNonExpired(),
                user.isAccountNonLocked(),
                user.getAuthorities()
        );

        userId = userDto.getId();
        email = userDto.getEmail();
    }

    public UserEx(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final Collection<? extends GrantedAuthority> authorities
    ) {
        super(login, password, authorities);
    }

    public UserEx(
            @NotNull final String login,
            @NotNull final String password,
            final boolean enabled,
            final boolean accountNonExpired,
            final boolean credentialsNonExpired,
            final boolean accountNonLocked,
            @NotNull final Collection<? extends GrantedAuthority> authorities
    ) {
        super(login, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }

}
