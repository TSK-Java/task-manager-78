package ru.tsc.kirillov.tm.exception.entity;

public final class EntityEmptyException extends AbstractEntityException {

    public EntityEmptyException() {
        super("Ошибка! Сущность не задана.");
    }

}
