package ru.tsc.kirillov.tm.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.kirillov.tm.api.service.IProjectService;
import ru.tsc.kirillov.tm.config.DatabaseConfiguration;
import ru.tsc.kirillov.tm.config.WebApplicationConfiguration;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;
import ru.tsc.kirillov.tm.marker.UnitCategory;
import ru.tsc.kirillov.tm.util.UserUtil;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, DatabaseConfiguration.class})
public class ProjectControllerTest {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext context;

    @NotNull
    private final ProjectDto projectDelete = new ProjectDto("id", "Name");

    @NotNull
    private final ProjectDto projectEdit = new ProjectDto("id", "Name");

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        projectService.save(UserUtil.getUserId(), projectDelete);
        projectService.save(UserUtil.getUserId(), projectEdit);
    }

    @After
    public void clean() {
        projectService.clear(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void create() {
        @NotNull final String url = "/project/create";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertEquals(3, projectService.count(UserUtil.getUserId()));
    }

    @Test
    @SneakyThrows
    public void delete() {
        @NotNull final String url = String.format("/project/delete/%s", projectDelete.getId());
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(2, projectService.count(userId));
        Assert.assertTrue(projectService.existsById(userId, projectDelete.getId()));
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertEquals(1, projectService.count(userId));
        Assert.assertFalse(projectService.existsById(userId, projectDelete.getId()));
    }

    @Test
    @SneakyThrows
    public void edit() {
        @NotNull final String url = String.format("/project/edit/%s", projectEdit.getId());
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @SneakyThrows
    public void list() {
        @NotNull final String url = "/projects";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

}
