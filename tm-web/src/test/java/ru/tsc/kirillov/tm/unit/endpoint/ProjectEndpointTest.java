package ru.tsc.kirillov.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.kirillov.tm.config.DatabaseConfiguration;
import ru.tsc.kirillov.tm.config.WebApplicationConfiguration;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;
import ru.tsc.kirillov.tm.marker.UnitCategory;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, DatabaseConfiguration.class})
public class ProjectEndpointTest {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext context;

    @NotNull
    private static final String PROJECT_URL = "http://localhost:8080/api/project/";

    @NotNull
    private final ProjectDto projectDelete = new ProjectDto("id", "Name");

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        save(projectDelete);
    }

    @After
    public void clean() {
        clear();
    }

    @SneakyThrows
    private void clear() {
        @NotNull final String url = PROJECT_URL + "clear";
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .delete(url)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void save(@NotNull final ProjectDto project) {
        @NotNull final String url = PROJECT_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(project);
        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Nullable
    @SneakyThrows
    private ProjectDto findById(@NotNull final String id) {
        @NotNull String url = PROJECT_URL + "findById/" + id;
        @NotNull final String json =
                mockMvc.perform(
                        MockMvcRequestBuilders.get(url)
                                .contentType(MediaType.APPLICATION_JSON)
                        )
                        .andDo(print())
                        .andExpect(status().isOk())
                        .andReturn()
                        .getResponse()
                        .getContentAsString();
        if (json.isEmpty()) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, ProjectDto.class);
    }

    @NotNull
    @SneakyThrows
    private List<ProjectDto> findAll() {
        @NotNull final String url = PROJECT_URL + "findAll";
        @NotNull final String json = mockMvc.perform(
                MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, ProjectDto[].class));
    }

    @SneakyThrows
    private long count() {
        @NotNull final String url = PROJECT_URL + "count";
        @NotNull final String json = mockMvc.perform(
                        MockMvcRequestBuilders.get(url)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        if (json.isEmpty()) return 0;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Long.class);
    }

    @Test
    public void findAllTest() {
        @NotNull final List<ProjectDto> projectList = findAll();
        Assert.assertEquals(1, projectList.size());
    }

    @Test
    public void findById() {
        @Nullable final ProjectDto project = findById(projectDelete.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(projectDelete.getId(), project.getId());
    }

    @Test
    @SneakyThrows
    public void existsById() {
        @NotNull String url = PROJECT_URL + "existsById/" + projectDelete.getId();
        @NotNull final String json =
                mockMvc.perform(
                                MockMvcRequestBuilders.get(url)
                                        .contentType(MediaType.APPLICATION_JSON)
                        )
                        .andDo(print())
                        .andExpect(status().isOk())
                        .andReturn()
                        .getResponse()
                        .getContentAsString();
        if (json.isEmpty()) return;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Boolean exists = objectMapper.readValue(json, Boolean.class);
        Assert.assertEquals(true, exists);
    }

    @Test
    public void add() {
        @NotNull final ProjectDto projectAdd = new ProjectDto("id", "Name");
        save(projectAdd);
        @Nullable final ProjectDto project = findById(projectAdd.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(projectAdd.getId(), project.getId());
    }

    @Test
    @SneakyThrows
    public void delete() {
        Assert.assertEquals(1, count());
        @NotNull final String url = PROJECT_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(projectDelete);
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .delete(url)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(0, count());
    }

    @Test
    @SneakyThrows
    public void deleteAll() {
        Assert.assertEquals(1, count());
        @NotNull final ProjectDto projectAdd = new ProjectDto("id", "Name");
        save(projectAdd);
        Assert.assertEquals(2, count());
        @NotNull final List<ProjectDto> projects = Collections.singletonList(projectAdd);
        @NotNull final String url = PROJECT_URL + "deleteAll";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(projects);
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .delete(url)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(1, count());
    }

    @Test
    public void clearTest() {
        Assert.assertEquals(1, count());
        @NotNull final ProjectDto projectAdd = new ProjectDto("id", "Name");
        save(projectAdd);
        Assert.assertEquals(2, count());
        clear();
        Assert.assertEquals(0, count());
    }

    @Test
    @SneakyThrows
    public void deleteById() {
        Assert.assertEquals(1, count());
        @NotNull final String url = PROJECT_URL + "deleteById/" + projectDelete.getId();
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .delete(url)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(0, count());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(1, count());
    }

}
