package ru.tsc.kirillov.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.kirillov.tm.config.DatabaseConfiguration;
import ru.tsc.kirillov.tm.config.WebApplicationConfiguration;
import ru.tsc.kirillov.tm.dto.model.TaskDto;
import ru.tsc.kirillov.tm.marker.UnitCategory;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, DatabaseConfiguration.class})
public class TaskEndpointTest {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext context;

    @NotNull
    private static final String TASK_URL = "http://localhost:8080/api/task/";

    @NotNull
    private final TaskDto taskDelete = new TaskDto("id", "Name");

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        save(taskDelete);
    }

    @After
    public void clean() {
        clear();
    }

    @SneakyThrows
    private void clear() {
        @NotNull final String url = TASK_URL + "clear";
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .delete(url)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void save(@NotNull final TaskDto task) {
        @NotNull final String url = TASK_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(task);
        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Nullable
    @SneakyThrows
    private TaskDto findById(@NotNull final String id) {
        @NotNull String url = TASK_URL + "findById/" + id;
        @NotNull final String json =
                mockMvc.perform(
                        MockMvcRequestBuilders.get(url)
                                .contentType(MediaType.APPLICATION_JSON)
                        )
                        .andDo(print())
                        .andExpect(status().isOk())
                        .andReturn()
                        .getResponse()
                        .getContentAsString();
        if (json.isEmpty()) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, TaskDto.class);
    }

    @NotNull
    @SneakyThrows
    private List<TaskDto> findAll() {
        @NotNull final String url = TASK_URL + "findAll";
        @NotNull final String json = mockMvc.perform(
                MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, TaskDto[].class));
    }

    @SneakyThrows
    private long count() {
        @NotNull final String url = TASK_URL + "count";
        @NotNull final String json = mockMvc.perform(
                        MockMvcRequestBuilders.get(url)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        if (json.isEmpty()) return 0;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Long.class);
    }

    @Test
    public void findAllTest() {
        @NotNull final List<TaskDto> taskList = findAll();
        Assert.assertEquals(1, taskList.size());
    }

    @Test
    public void findById() {
        @Nullable final TaskDto task = findById(taskDelete.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(taskDelete.getId(), task.getId());
    }

    @Test
    @SneakyThrows
    public void existsById() {
        @NotNull String url = TASK_URL + "existsById/" + taskDelete.getId();
        @NotNull final String json =
                mockMvc.perform(
                                MockMvcRequestBuilders.get(url)
                                        .contentType(MediaType.APPLICATION_JSON)
                        )
                        .andDo(print())
                        .andExpect(status().isOk())
                        .andReturn()
                        .getResponse()
                        .getContentAsString();
        if (json.isEmpty()) return;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Boolean exists = objectMapper.readValue(json, Boolean.class);
        Assert.assertEquals(true, exists);
    }

    @Test
    public void add() {
        @NotNull final TaskDto taskAdd = new TaskDto("id", "Name");
        save(taskAdd);
        @Nullable final TaskDto task = findById(taskAdd.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(taskAdd.getId(), task.getId());
    }

    @Test
    @SneakyThrows
    public void delete() {
        Assert.assertEquals(1, count());
        @NotNull final String url = TASK_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(taskDelete);
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .delete(url)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(0, count());
    }

    @Test
    @SneakyThrows
    public void deleteAll() {
        Assert.assertEquals(1, count());
        @NotNull final TaskDto taskAdd = new TaskDto("id", "Name");
        save(taskAdd);
        Assert.assertEquals(2, count());
        @NotNull final List<TaskDto> tasks = Collections.singletonList(taskAdd);
        @NotNull final String url = TASK_URL + "deleteAll";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(tasks);
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .delete(url)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(1, count());
    }

    @Test
    public void clearTest() {
        Assert.assertEquals(1, count());
        @NotNull final TaskDto taskAdd = new TaskDto("id", "Name");
        save(taskAdd);
        Assert.assertEquals(2, count());
        clear();
        Assert.assertEquals(0, count());
    }

    @Test
    @SneakyThrows
    public void deleteById() {
        Assert.assertEquals(1, count());
        @NotNull final String url = TASK_URL + "deleteById/" + taskDelete.getId();
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .delete(url)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(0, count());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(1, count());
    }

}
